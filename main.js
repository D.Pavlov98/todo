let list=document.querySelector('.js-group');
let todo;
function toLocal(){
    todo=list.innerHTML;
    localStorage.setItem('todo',todo);
}
const inputEl=document.querySelector('.js-input');
const btnAddEl=document.querySelector('.js-btn-add');
let arrSpan=0;
let textSpan='';
let liShadow;
let check=false;
btnAddEl.addEventListener('click',function(){
    if(btnAddEl.textContent==='Добавить'){
    const newLiEl=createNewLi(inputEl.value||'пустая задача');
    document.querySelector('.js-group').append(newLiEl);
    inputEl.value='';
    toLocal();
    return newLiEl;}
})

document.querySelector('.js-group').addEventListener('click',event=>{
    if(event.target.classList.contains('js-checkbox')){
        event.target.parentNode.querySelector('.js-item-value').classList.toggle('item-value');
    }
    if(check===false && event.target.classList.contains('js-remove')){
        event.target.parentNode.remove();
        toLocal();
    }
})
document.querySelector('.panel').addEventListener('click',event=>{
    if(event.target.classList.contains('edit')){
    inputEl.value='';
    
    removeEditDiv();
    }
})
function createEditDiv(){
    const newEditDiv=document.createElement('div');
    newEditDiv.classList.add('edit');
    newEditDiv.textContent='X';
    document.querySelector('.panel').append(newEditDiv);
}

function removeEditDiv(){
    document.querySelector('.edit').remove();
    btnAddEl.classList.toggle('btn-color');
    btnAddEl.textContent='Добавить';
    liShadow.parentNode.classList.toggle('li-edit');
    check=false;
}

document.querySelector('.js-group').addEventListener('click',event=>{
    if(check===false && event.target.classList.contains('js-item-value')){
        createEditDiv();
        liShadow=event.target.parentNode;
        liShadow.parentNode.classList.toggle('li-edit');
textSpan=event.target.parentNode.querySelector('.js-item-value').textContent;
inputEl.value=textSpan;
btnAddEl.classList.toggle('btn-color');
    btnAddEl.textContent='Обновить';
    let edit=event.target.parentNode.querySelector('.js-item-value');
    const nodeList=document.querySelectorAll('span');
    const result=Array.from(nodeList);
    
    result.forEach(function(edit,index){
        return index;
        
    })
    arrSpan=result.indexOf(edit);
    console.log(arrSpan);
    check=true;

    }
    
})
btnAddEl.addEventListener('click',()=>{
    if(btnAddEl.textContent==='Обновить'){
        document.querySelectorAll('span')[arrSpan].textContent=inputEl.value;
        console.log(inputEl.value);
        removeEditDiv();
        toLocal();
    
    inputEl.value='';
    }
    
})


if(localStorage.getItem('todo')){
    list.innerHTML=localStorage.getItem('todo');

};



function createNewLi(text){
const newLi=document.createElement('li');
newLi.classList.add('list-group-item');
newLi.classList.add('js-item');    
const newDiv=document.createElement('div');
newDiv.classList.add('form-group');
const newCheckBox=document.createElement('input');
newCheckBox.classList.add('js-checkbox');
newCheckBox.classList.add('form-check-input');
newCheckBox.setAttribute('type','checkbox');
newSpan=document.createElement('span');
newSpan.classList.add('js-item-value');
newSpan.textContent=text;
newRemove=document.createElement('div');
newRemove.classList.add('remove');
newRemove.classList.add('js-remove');
newRemove.textContent='X';
newRemove.setAttribute('title','Удалить из списка')
newLi.append(newDiv);
newLi.append(newRemove);
newDiv.append(newCheckBox);
newDiv.append(newSpan);
return newLi;
}


{/* <body>
<div class="container">
    <div class="panel">
        <input class="form-control js-input" id="exampleFormControlInput1" placeholder="Новая задача">
        <button type="button" class="btn btn-primary js-btn-add">Добавить</button>
    </div>

<ul class="list-group js-group">
    <li class="list-group-item js-item">
        <div class="form-group">
            <input class="form-check-input js-checkbox" type="checkbox" value="" id="flexCheckDefault">
            <span class="js-item-value">An item</span>
        </div>
        <div class="remove js-remove" title="Удалить из списка">X</div>
    </li>
</ul> */}